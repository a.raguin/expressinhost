# I- Resources of the project
    
## Source files
**Window.cpp**   (makes the graphical interface)

**Process_funct.cpp**   (processes the input sequences)

**Process_tables.cpp**   (processes the input raw data tables)
    
**Tables/**   (folder that contains raw data tables used as input files)
	
## Example input files
**Rad51_CLUSTAL.txt**   (input amino acid sequences)

**Rad51_nucleotide.txt**   (input nucleotide sequences)

See instructions for an example in **V- EXAMPLE RUN**
    

## For Windows distributions
**express_in_host_v0.1-dev.zip** (Executable file with .dll dependencies)



# II- Purpose of the software

ExpressInHost is a GTK/C++ based graphical user interface (GUI) that allows to tune the codon sequence of a target protein towards its recombinant expression in a host microorganism. 

It offers three distinct tuning options/modes:

**i) Direct mapping**: this mode mimicks, for each sequence, the translation speed profile from the native organisms into the selected host. 

**ii) Optimisation and conservation 1**: this mode maximises, for each sequence, the translation speed for each codon excepted those where amino acids are highly conserved among the orthologous sequences.

**iii) Optimisation and conservation 2**: this mode maximises, for each sequence, the translation speed for each codon excepted those where slow translation speed is highly conserved among the orthologous sequences. 

**IMPORTANT: The GUI is accompanied by an "Instructions Window" that guides the user through the software, to fill the successive _steps_ of the GUI.**


# III- Setup

## 1) Installation
   
### Linux: 
   
   Install the libraries **gtkmm-3.0** for your Linux distribution. For Ubuntu install libgtkmm-3.0-dev: "sudo apt install libgtkmm-3.0-dev"

### Windows: 
   
   Download and install Visual Studio and vcpkg

   #### Download and install c++ redistributable for visual studio 2019

   https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads

   https://aka.ms/vs/16/release/vc_redist.x64.exe

   #### Download and install vcpkg

   https://docs.microsoft.com/en-us/cpp/build/vcpkg?view=vs-2019

   https://github.com/Microsoft/vcpkg

  - Install gtk via vcpkg:   ">vcpkg search gtk"

atkmm                2.24.2-2         atkmm is the official C++ interface for the ATK accessibility toolkit library....

gtk                  3.22.19-3        Portable library for creating graphical user interfaces.

gtkmm                3.22.2-2         gtkmm is the official C++ interface for the popular GUI library GTK+.

scintilla            4.2.3            A free source code editing component for Win32, GTK+, and OS X
   
  - If your library is not listed, please open an issue at and/or consider making a pull request:
    https://github.com/Microsoft/vcpkg/issues

   - vcpkg install gtk:x86-windows:    "vcpkg integrate install" (It applies user-wide integration for this vcpkg root).

All MSBuild C++ projects can now include any installed libraries.

Linking will be handled automatically.

Installing new libraries will make them instantly available.


For the problematic icons in the Adwaita theme, refer to this thread:
https://github.com/microsoft/vcpkg/issues/4417


## 2) Run: Download the Release and ...

**Windows**:
   
- Extract the files from **express_in_host_v0.1-dev.zip**
- Inside the **Release/** folder extracted, add the following folder:

**Tables/**  (folder that contains the raw data tables used as input files)
        
Any input file (nucleotide sequences or amino acid sequences) should also be stored in the same folder  as **express_in_host.exe**, for instance the **Example files** **Rad51_CLUSTAL.txt** and **Rad51_nucleotide.txt**
- Double click on **express_in_host.exe** and **CAREFULLY READ** the instructions provided in the **Instructions Window**.
   
**Linux**:
    
- Ensure that the **Source files**, the folder **Tables/** and the **Example files** (or any input nucleotide sequence or amino acide sequence files) are all in the same directory.  
- Open a terminal and go to the directory where your files are.
- Compile the codes using 

**g++ Window.cpp Process_funct.cpp Process_tables.cpp -o express_in_host \`pkg-config gtkmm-3.0 --cflags --libs\`**
 
- Run the program with the command **./express_in_host** and **CAREFULLY READ** the instructions provided in the **Instructions Window**.
   
  
## 3) Input files  
  
The input files requested depend on the mode to be selected and are detailed in the **Instructions Window**.

The file that contains the nucleotide sequences and (if requested) the file that contains the amino acid sequences must both be stored in the same folder as the executable **express_in_host**.

Any additional raw data table prepared by the user must be stored in the folder **Tables/**
	
## 4) Output files

The software is finished processing the sequences when the name of the output files appear directly on the graphical interface (in **_steps 8_** and **_9_**).

The outpuf files created are located in the same folder as the executable **express_in_host**.
   

# IV- CREATE tRNA TABLE FOR ADDITIONAL NATIVE ORGANISMS


**If the native organism of a gene of interest is not listed in the dropdown list in **_step 3_**, you need to prepare the table of raw data for that organism.**

To build the table of raw data: 
- Enter the folder **Tables/**
- Copy-paste any of the tables present to use it as a template. 
- Rename it, following the instructions for the name of the input files in **Instructions Window**, example: **My_organism.txt**
- Any raw data table contains: column 1: amino acid, column 2: anti-codon, column 3: codon, column 4: tRNA GCN, column 5: codon used in case of wobble.
- Only column 4 and 5 need to be adapted to your specific organism. To find the tRNA GCN for your organism of interest, use databases such as "**GtRNAdb: Genomic tRNA Database**".
- Modify the values of the tRNA GCN in column 4. In case the tRNA GCN = 0, use the wobble base pairing to determine the codon with which one the codon that has no tRNA will share its tRNA.

   **Example of wobble base pairing**: 
   - Open the table Xenopus_tropicalis.txt provided in the folder **Tables/** 
   - For Alanine, anti-codon GGC (second line, second column) has no GCN, so column 4 = 0, and column 5 = GCU, because the tRNA AGC, that reads GCU will now also decode GCC. 

When runing the software with the newly created tRNA table, simply follow the instructions given in **Instructions Window**:
- In **_step 3_**: select ELSE in the dropdown list at the line corresponding to the organism that needed the tRNA table.
- In **_step 4_**: enter the name of your new tRNA table on the same line.


# V- EXAMPLE RUN

To run test examples using the Example input files provided with the software:

## 1) To test the modes Optimisation and conservation 1 or Optimisation and conservation 2

- Double click the executable file **express_in_host.exe**

- Read the instructions provided in the **Instructions Window**

- **_step 1_**: enter "**Rad51_CLUSTAL.txt**"

- **_step 2_**: enter "**Rad51_nucleotide.txt**"

- **_step 3_**: select the 10 native organisms of the **Rad51_CLUSTAL.txt** and **Rad51_nucleotide.txt** files, in the correct order: Staphylococcus_aureus, Methanocaldococcus_jannaschii, Caenorhabditis_elegans...

- **_step 4_**: nothing to be entered

- **_step 5_**: select one among the 4 possible hosts in which you wish to express the genes

- **_step 6_**: select either Optimisation and conservation 1 or Optimisation and conservation 2

- **_step 7_**: click Run!

After a few seconds the output files appear in **_steps 8_** and **_9_**.



## 2) To test the mode Direct mapping

- close the GUI window

- repeate each **_steps_** as in **1)** BUT for **_step 1_**: nothing to be entered

-----------------------------------------------------------------------------------------------
ATTRIBUTION PARTIES
-----------------------------------------------------------------------------------------------

The ExpressInHost software as described here has been developed by Adélaïde Raguin.
The work has been carried out at the University of Aberdeen, UK, in the context of the project "Predictive optimization of biocatalyst production for high-value chemical manufacturing".
It has been supported by Innovate UK, and supervised by Prof. Maria Carmen Romano and Prof. Ian Stansfield.

-----------------------------------------------------------------------------------------------
AUTHORSHIP
-----------------------------------------------------------------------------------------------
The author of the ExpressInHost software is Adélaïde Raguin.

-----------------------------------------------------------------------------------------------
CORRESPONDENCE
-----------------------------------------------------------------------------------------------
adelaide.raguin@hhu.de

m.romano@abdn.ac.uk


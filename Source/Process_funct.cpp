#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <time.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <string.h>
#include <list>
#include <math.h>
#include <cstring>
#include <errno.h>
#include <iso646.h>

using namespace  std;
using std::ifstream;
using std::ofstream;
using std::ios;


#define conservation_threshold 0.75//For the mode 3 = "Optimisation and Conservation 2,"
//percentage that defines the amount of sequences among the orthologous set that must show a slow codon at a given position for that position to be tagged "slow and conserved".

int test, test2, test3, i, m, n, r, nbr_lines_in_block, tot_nbr_lines, length_line_block1, length_seq_block1, length_line_last_block, length_seq_last_block, counter_similarity;
float col6, col7, Rank2, Delta;
string ch_A("A"), ch_C("C"), ch_G("G"), ch_T("T"), ch_U("U"), ch_S("S"), ch_0("0"), stop_codon("UAA");
string w_space(" "), s_asterix("*"), hyphen("-"), triple_hyphen("---"), alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ-");
string line, line2, line3, line4, line6, x, y, col1, col2, col3, col4, col5, col8;
string letter1, letter2, letter3, input_codon, host_table, amino_acid, output_codon, Symb;
string seq_clustal_file, sequences_nucleotides_file;
vector<string> sequences, potential_codon, selected_codon;
vector<float> potential_rank, cpt_symbols;
string output_debug, Para_mode;


void find_files()
{
//Find the input files: Clustal alignment (if mode 2 and 3) and nucleotide sequences (for any mode)
    ifstream file2_0("Input_file_names.txt", ios::in);
    if(file2_0)
    {
        line.clear();
        seq_clustal_file.clear();
        sequences_nucleotides_file.clear();
        i=0;
        while(getline(file2_0,line))
        {
            if(Para_mode.compare("Direct_mapping") != 0)
            {
                if(i==0)
                    seq_clustal_file = line;
                if(i==1)
                    sequences_nucleotides_file = line;
            }
            if(Para_mode.compare("Direct_mapping") == 0)
            {
                if(i==0)
                    sequences_nucleotides_file = line;
            }
            i++;
        }
    }

}

void seq_clustal_formating()
{
//Clear the spacing and the numbers at the end of each line
    line.clear();
    ifstream file2_1(seq_clustal_file, ios::in);
    test2=0;
    i=0;
    if(file2_1)
    {
        while(getline(file2_1,line))
        {
            //Count the number of lines per block
            //Check that this is a line that starts with the name of an organism, and not a line of symbols
            test=0;
            if(line.size()>0)
            {
                for(int w=0; w<alphabet.size(); w++)
                {
                    x = line.at(0);
                    letter1 = alphabet.at(w);
                    if(x.compare(letter1) == 0)
                        test=1;
                }
            }
            //If the end of the first block (flagged by test2) has not been found and the start of the line is a letter
            if((test2==0) && (test==1))
                nbr_lines_in_block++;
            //If the end of the first block (flagged by test2) has not been found and the line does not start with a letter
            if((test2==0) && (test==0))
            {
                test2=1;//End of the first block found
                nbr_lines_in_block++;//Count the symbol line
            }
            //Find the length of the first line and of the first piece of sequence
            if(i==0)
            {
                //Clean the end of the first line
                test=0;
                while(test==0)//while the end of the line is not an hyphen or a letter
                {
                    //If the end of the line is an hyphen or a letter, test=1
                    for(int w=0; w<alphabet.size(); w++)
                    {
                        x = line.at(line.size()-1);
                        letter1 = alphabet.at(w);
                        if(x.compare(letter1) == 0)
                            test=1;
                    }
                    //If the end of the line is not an hyphen or a letter erase it
                    if(test==0)
                        line.erase(line.end()-1);


                }

                //After its end is cleaned, the length_line_block1 is the length of the first line
                length_line_block1 = line.size();//Measure the length of the first line in block 1
                //Find the length of a sequence (without header part) in block 1
                for(int t =0; t<length_line_block1-1; t++)
                {
                    x = line.at(t);
                    y = line.at(t+1);
                    for(int w=0; w<alphabet.size(); w++)
                    {
                        letter1 = alphabet.at(w);
                        if((x.compare(w_space) == 0) && (y.compare(letter1) == 0))
                            length_seq_block1 = length_line_block1 -(t+1);
                    }
                }
            }
            //Clean the end of all lines based on the length of the first line cleaned
            //If the last block is shorter that will not be cleaned yet
            while(line.size()>length_line_block1)
                line.erase(line.end()-1);


            ofstream file2_3_0("modif_sequences_1.txt", ios::app);
            //Printout the shortened sequences as well as the symbol sequences
            if((line.size()!=0) && (line.size()!=1))
            {
                file2_3_0<<line<<endl;//In file2_3_0 only store the lines with either letters or symbols
                tot_nbr_lines++;
            }
            i++;
        }
    }
    else
        output_debug = output_debug + "In STEP 1 the file with the file name entered could not be found in the folder." + '\n'+ '\n';

    //If no symbols are on the last line of the last block that line has been removed and needs to be added
    ifstream file2_4_0("modif_sequences_1.txt", ios::in);
    if(file2_4_0)
    {
        line.clear();
        i=0;

        while(getline(file2_4_0,line))
        {
            test=0;//Test=1 if the last line starts with a letter
            if(i==tot_nbr_lines-1)//Last line of the last block
            {
                x=line.at(0);
                for(int w=0; w<alphabet.size(); w++)
                {
                    letter1 = alphabet.at(w);
                    if(x.compare(letter1) == 0)
                        test=1;
                }
            }
            if(test==1)//If a letter has been found on the last line
            {
                ofstream file2_3_1("modif_sequences_1.txt", ios::app);
                //Add a last line made of three empty spaces
                file2_3_1<<"   "<<endl;
                tot_nbr_lines++;
            }
            i++;
        }
    }


    if(nbr_lines_in_block == 0)
        output_debug = output_debug + "In STEP 1 the clustal alignment file is empty." + '\n'+ '\n';

    if(nbr_lines_in_block > 11)
        output_debug = output_debug + "In STEP 1 the clustal alignment file contains more than 10 sequences, the maximum allowed." + '\n'+ '\n';


    //Initiate the vector of sequences
    for(int t=0; t<nbr_lines_in_block; t++)
        sequences.push_back("");

    ifstream file2_4("modif_sequences_1.txt", ios::in);
    if(file2_4)
    {
        line.clear();
        i=0;
        while(getline(file2_4,line))
        {
            if(i==tot_nbr_lines-nbr_lines_in_block)//First line of the last block
            {
                //Clean its end
                test=0;
                while(test==0)//while the end of the line is not an hyphen or a letter
                {
                    //If the end of the line is an hyphen or a letter, test=1
                    for(int w=0; w<alphabet.size(); w++)
                    {
                        x = line.at(line.size()-1);
                        letter1 = alphabet.at(w);
                        if(x.compare(letter1) == 0)
                            test=1;
                    }
                    //If the end of the line is not an hyphen or a letter erase it
                    if(test==0)
                        line.erase(line.end()-1);
                }

                //Measure the length of the first line of the last block
                length_line_last_block = line.size();

                //Measure the sequence of the first line of the last block
                for(int t=0; t<length_line_last_block-1; t++)
                {
                    x = line.at(t);
                    y = line.at(t+1);
                    for(int w=0; w<alphabet.size(); w++)
                    {
                        letter1 = alphabet.at(w);
                        if((x.compare(w_space) == 0) && (y.compare(letter1) == 0))
                            length_seq_last_block = length_line_last_block -(t+1);
                    }
                }
            }
            //For the lines of the last block
            if(i>=tot_nbr_lines-nbr_lines_in_block)
            {
                //Clean the end of the lines if required
                while(line.size()>length_line_last_block)
                    line.erase(line.end()-1);
                //For the last line of the last block
                if((i==tot_nbr_lines-1) && (line.size()<length_line_last_block))
                {
                    //Make it of the same length than the first line of the last block if required
                    while(line.size()<length_line_last_block)
                        line.append(" ");
                }

            }

            //Concatenate sequences as one sequence per line for all blocks

            //For all blocks excepted the last one
            if(i<tot_nbr_lines-nbr_lines_in_block)
            {
                for(int t=0; t<nbr_lines_in_block; t++)
                {
                    if(i % nbr_lines_in_block == t)
                        sequences.at(t) = sequences.at(t) + line.substr(length_line_block1-length_seq_block1,length_seq_block1);
                }
            }
            //For the last block
            if(i>=tot_nbr_lines-nbr_lines_in_block)
            {
                for(int t=0; t<nbr_lines_in_block; t++)
                {
                    if(i% nbr_lines_in_block == t)
                        sequences.at(t) = sequences.at(t) + line.substr(length_line_last_block-length_seq_last_block,length_seq_last_block);
                }
            }
            i++;

        }
    }

    ofstream file2_6("modif_sequences_2.txt", ios::app);
    ofstream file2_8("modif_sequences_3.txt", ios::app);
    //Write sequences in corresponding files
    for(int t=0; t<nbr_lines_in_block; t++)
    {
        //Printout the nucleotide sequences
        if(t<nbr_lines_in_block-1)
            file2_6<<sequences.at(t)<<endl;

        //Printout the symbols sequence
        if(t==nbr_lines_in_block-1)
            file2_8<<sequences.at(t);
    }
}

void clear_nucleotide_sequences()
{

//In case the nucleotide sequences are not a sequence par line, concatenate them
    ifstream file2_1_0(sequences_nucleotides_file, ios::in);
    if(file2_1_0)
    {
        i=0;
        line.clear();
        line2.clear();
        test2=0;//Flags that a sequence has been concatenated

        while(getline(file2_1_0,line))
        {
            test=0;//Flags the presence of a letter at the begining of a sequence
            //If the beginning of the line is an hyphen or a letter, test=1
            for(int w=0; w<alphabet.size(); w++)
            {
                x = line.at(0);
                letter1 = alphabet.at(w);
                if(x.compare(letter1) == 0)
                    test=1;
            }
            //If the line starts with a letter
            if(test==1)
            {
                test3=0;
                //Clean its end
                while(test3==0)//while the end of the line is not an hyphen or a letter
                {
                    //If the end of the line is an hyphen or a letter, test3=1
                    for(int w=0; w<alphabet.size(); w++)
                    {
                        x = line.at(line.size()-1);
                        letter1 = alphabet.at(w);
                        if(x.compare(letter1) == 0)
                            test3=1;
                    }
                    //If the end of the line is not an hyphen or a letter erase it
                    if(test3==0)
                        line.erase(line.end()-1);
                }

                line2 = line2 +line;
                test2=1;

            }
            //If the line does not start with a letter but a sequence has been concatenated
            if((test==0) and (test2==1))
            {
                //Ensure that the sequence contains only U, A, G anc C
                for(int x=0; x<line2.size(); x++)
                {
                    letter1 = line2.at(x);
                    if(letter1.compare(ch_T)==0)
                        line2.replace(x,1,ch_U);//Convert any "T" into "U"
                    if((letter1.compare(ch_T)!=0) and (letter1.compare(ch_U)!=0) and (letter1.compare(ch_A)!=0) and (letter1.compare(ch_C)!=0) and (letter1.compare(ch_G)!=0))
                        output_debug = output_debug + "In STEP 2 in a nucleotide sequence a character different from U, T, A, G and C is present." + '\n'+ '\n';

                }
                letter1 = line2.at(line2.size()-3);
                letter2 = line2.at(line2.size()-2);
                letter3 = line2.at(line2.size()-1);
                //Remove stop codon UAA
                if((letter1.compare(ch_U)==0) and (letter2.compare(ch_A)==0) and (letter3.compare(ch_A)==0))
                    line2.erase(line2.size()-3,3);
                //Remove stop codon UAG
                if((letter1.compare(ch_U)==0) and (letter2.compare(ch_A)==0) and (letter3.compare(ch_G)==0))
                    line2.erase(line2.size()-3,3);
                //Remove stop codon UGA
                if((letter1.compare(ch_U)==0) and (letter2.compare(ch_G)==0) and (letter3.compare(ch_A)==0))
                    line2.erase(line2.size()-3,3);

                //Printout the concatenated nucleotide sequences
                ofstream file2_2_0("modif_sequences_4.txt", ios::app);
                file2_2_0<<line2<<endl;//In file2_2_0 only store the lines with either letters or symbols
                line2.clear();
                test2=0;
                i++;
            }
        }
    }
    else
        output_debug = output_debug + "In STEP 2 the file with the file name entered could not be found in the folder, or you entered a name in STEP 1 while you selected the mode Direct mapping." + '\n'+ '\n';


    if((i!=nbr_lines_in_block-1) and (Para_mode.compare("Direct_mapping") != 0))
        output_debug = output_debug + "In STEP 1 and STEP 2 the number of sequences in the clustal alignment file and the number of sequences in the nucleotide sequences file are different." + '\n'+ '\n';

    if(i>10)
        output_debug = output_debug + "In STEP 2 the nucleotide sequences file contains more than 10 sequences, the maximum allowed." + '\n'+ '\n';

}

void alignement_seq_nucleo()
{
//Reproduce the alignment of the amino acid sequences (provided as a Clustal alignment) into the nucleotide sequences by introducing the corresponding triplets of hyphens
    ifstream file2_11("modif_sequences_2.txt", ios::in);
    if(file2_11)
    {
        n=0;//index of the line in the nucleotides file
        i=0;//index of the line in the amino acids file
        m=0;//counts the number of amino acis already considered for the alignment (NOT the hyhens)
        line.clear();
        line2.clear();
        line3.clear();
        while(getline(file2_11,line))//Read aligned amino acid sequences one by one (= line by line)
        {
            i++;
            ifstream file2_12("modif_sequences_4.txt", ios::in);
            if(file2_12)
            {
                n=0;
                line2.clear();
                while(getline(file2_12,line2))//Read nucleotide sequences one by one (= line by line)
                {
                    n++;
                    if(i==n)//When the sequences are corresponding
                    {
                        m=0;
                        line3.clear();
                        for(int t=0; t<line.size(); t++)
                        {
                            letter1 = line.at(t);
                            if(letter1.compare(hyphen)==0)//If there is an hyphen in the amino acid sequence
                                line3.append("---");//Introduce three hyphens in the nucleotide sequence
                            else//If there is an amino acid, collect the corresponding codon (3 letters) in the nucleotide sequence
                            {
                                letter1 = line2.at(3*m);
                                letter2 = line2.at(3*m+1);
                                letter3 = line2.at(3*m+2);
                                line3.append(letter1);
                                line3.append(letter2);
                                line3.append(letter3);
                                m++;
                            }
                        }

                        ofstream file2_14("modif_sequences_5.txt", ios::app);//Output the newly aligned nucleotide sequence and add the stop codon UAA
                        file2_14<<line3<<"UAA"<<endl;
                    }
                }
            }
        }
    }
}

void Algo1()
{
//Core function of the mode 1 = "Direct mapping"
    ifstream file2_15("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host (the last one)
    if(file2_15)
    {
        i=0;
        line.clear();
        while(getline(file2_15, line))//Count the number of lines = number of natives +1 (host)
            i++;
    }

    if(i<2)//Nbr natives = nbr_lines_in_block-1, but here the host line is also counted, so at least 1 native and 1 host should be in Table_names.txt
        output_debug = output_debug + "In STEP 3 STEP 4 and STEP 5 either the native organisms and/or the host have not been entered." + '\n'+ '\n';

    n=0;
    line.clear();
    ifstream file2_15_2("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_15_2, line))//Re-read the file that contains the name of all tables
    {
        n++;
        if(n==i)//When the last line is reached
            host_table = "Tables/Processed_"+line;//Store the name of the host table

    }

    /****** Make a loop on all natives ******/
    i=0;
    line.clear();
    ifstream file2_15_3("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_15_3, line))//Re-read the file that contains the name of all tables
    {
        i++;
        ifstream file2_17("modif_sequences_4.txt", ios::in);//Open the aligned nucleotide sequences file
        if(file2_17)
        {
            n=0;
            line2.clear();
            while(getline(file2_17, line2))//For each sequence
            {
                n++;
                input_codon.clear();
                line3.clear();
                if(i==n)//If the index of the table corresponds to the one of the sequence
                {
                    for (int t=0; t<line2.size()/(float)3.0; t++)//Read the sequence codon after codon
                    {
                        letter1 = line2.at(3*t);
                        letter2 = line2.at(3*t+1);
                        letter3 = line2.at(3*t+2);
                        input_codon = letter1+letter2+letter3;

                        if (input_codon.compare(triple_hyphen)==0)//If there is a triple hyphen, we introduce it in the processed sequence
                            line3.append(triple_hyphen);

                        if (input_codon.compare(triple_hyphen)!=0)//If not, we search for corresponding codons
                        {
                            test=0;
                            //Open the processed native table
                            ifstream file2_18("Tables/Processed_"+line, ios::in);//Open the processed native table, search and store the amino acid and the Rank of the native codon
                            line4.clear();
                            if(file2_18)
                            {
                                amino_acid.clear();
                                while(getline(file2_18,line4))
                                {
                                    istringstream in(line4);
                                    in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                    if(col3.compare(input_codon)==0)//When native codon is found
                                    {
                                        Rank2 = col6;//Store its rank
                                        amino_acid = col1;
                                        test=1;
                                    }
                                }
                            }
                            else
                                output_debug = output_debug + "In STEP 3 or STEP4: the table for the native:" + line + "could not be found."+ '\n' + "If you implemented this table yourself the format is not correct." + '\n' +"If this table is listed in the drop down menu of STEP 3, you might have erased it from the folder."+ '\n'+ '\n';

                            if(test==0)//If the codon is not in the table it is a stop codon
                                line3.append(stop_codon);

                            //If the codon is not a stop codon
                            else
                            {
                                //Search for the corresponding codon in the host table
                                ifstream file2_19(host_table, ios::in);//Open the host table
                                line6.clear();
                                if(file2_19)
                                {
                                    potential_codon.clear();
                                    potential_rank.clear();
                                    while(getline(file2_19,line6))//Read the host table
                                    {
                                        istringstream in(line6);
                                        in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                        if(col1.compare(amino_acid)==0)//If the amino acid is the same as the one of the native codon
                                        {
                                            potential_codon.push_back(col3);//Store the codon
                                            potential_rank.push_back(col6);//Store the rank
                                        }

                                    }
                                }
                                else
                                    output_debug = output_debug + "In STEP 5 the host table could not be found. Make sure it is stored in the folder Tables/" + '\n'+ '\n';

                                //Find the codon of rank closest to the one of the native codon
                                Delta=10000000.;//This will measure the distance of the potential ranks to the rank of the native codon
                                for(int l=0; l<potential_rank.size(); l++) //For all potential codons
                                {
                                    if(abs(potential_rank.at(l)-Rank2)<Delta)//Find the smallest distance to the native codon
                                        Delta = abs(potential_rank.at(l)-Rank2);
                                }

                                selected_codon.clear();

                                //Find all codons among the potential ones whose rank is at this distance to the native codon
                                for(int l=0; l<potential_rank.size(); l++) //For all potential codons
                                {
                                    if(abs(potential_rank.at(l)-Rank2)==Delta)//If distance to the Rank is equal the the smallest distance (Delta)
                                        selected_codon.push_back(potential_codon.at(l));//Subset that contains only the codons that can be corresponding codons. They are "equivalent" codons.
                                }

                                //Select randomly the corresponding codon among the equivalent ones
                                r = rand()%(int)(selected_codon.size());
                                output_codon=selected_codon.at(r);

                                //Add it to the processed sequence
                                line3.append(output_codon);
                            }
                        }
                    }

                    ofstream file2_20("modif_sequences_6.txt",ios::app);
                    file2_20<<line3<<endl;
                }
            }
        }
    }
}


void Algo2()
{
//Core function of the mode 2 = "Optimisation and Conservation 1"
    ifstream file2_21("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host (the last one)
    if(file2_21)
    {
        i=0;
        line.clear();
        while(getline(file2_21, line))//Count the number of lines = number of natives +1 (host)
            i++;
    }

    if(i!=nbr_lines_in_block)//Nbr natives = nbr_lines_in_block-1, but here the host line is also counted
        output_debug = output_debug + "In STEP 3 STEP 4 and STEP 5 either the native organisms and/or the host have not been entered." + '\n'+ '\n';

    n=0;
    line.clear();
    ifstream file2_21_2("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_21_2, line))//Re-read the file that contains the name of all tables
    {
        n++;
        if(n==i)//When the last line is reached
            host_table = "Tables/Processed_"+line;//Store the name of the host table

    }

    /****** Make a loop on all natives ******/
    i=0;
    line.clear();
    ifstream file2_21_3("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_21_3, line))//Re-read the file that contains the name of all tables
    {
        i++;
        ifstream file2_22("modif_sequences_5.txt", ios::in);//Open the aligned nucleotide sequences file
        if(file2_22)
        {
            n=0;
            line2.clear();
            while(getline(file2_22, line2))//For each sequence
            {
                n++;
                input_codon.clear();
                line3.clear();
                if(i==n)//If the index of the table corresponds to the one of the sequence
                {
                    for (int t=0; t<line2.size()/(float)3.0; t++)//Read the sequence codon after codon
                    {
                        letter1 = line2.at(3*t);
                        letter2 = line2.at(3*t+1);
                        letter3 = line2.at(3*t+2);
                        input_codon = letter1+letter2+letter3;

                        if (input_codon.compare(triple_hyphen)==0)//If there is a triple hyphen, we introduce it in the processed sequence
                            line3.append(triple_hyphen);

                        if (input_codon.compare(triple_hyphen)!=0)//If not, we search for corresponding codons
                        {
                            test=0;

                            //Open the processed native table
                            ifstream file2_23("Tables/Processed_"+line, ios::in);//Open the processed native table, search and store the amino acid and the rank of the native codon
                            line4.clear();
                            if(file2_23)
                            {
                                amino_acid.clear();
                                while(getline(file2_23,line4))
                                {
                                    istringstream in(line4);
                                    in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                    if(col3.compare(input_codon)==0)//When native codon is found
                                    {
                                        Rank2 = col6;//Store its rank
                                        amino_acid = col1;
                                        test=1;
                                    }
                                }
                            }
                            else
                                output_debug = output_debug + "In STEP 3 or STEP4: the table for the native:" + line + "could not be found."+ '\n' + "If you implemented this table yourself the format is not correct." + '\n' +"If this table is listed in the drop down menu of STEP 3, you might have erased it from the folder."+ '\n'+ '\n';

                            if(test==0)//If the codon is not in the table it is a stop codon
                                line3.append(stop_codon);

                            //If the codon is not a stop codon
                            else
                            {
                                //Find the corresponding codon that codes for the same amino acid (either the optimal one or the closest in rank)
                                ifstream file2_24(host_table, ios::in);//Open the host table
                                line6.clear();
                                if(file2_24)
                                {
                                    potential_codon.clear();
                                    potential_rank.clear();
                                    while(getline(file2_24,line6))//Read the host table
                                    {
                                        istringstream in(line6);
                                        in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                        if(col1.compare(amino_acid)==0)//If the amino acid is the same as the one of the native codon
                                        {
                                            potential_codon.push_back(col3);//Store the codon
                                            potential_rank.push_back(col6);//Store the rank
                                        }

                                    }
                                }
                                else
                                    output_debug = output_debug + "In STEP 5 the host table could not be found. Make sure it is stored in the folder Tables/" + '\n'+ '\n';

                                //Check whether the codon has to be optimised or conserved
                                ifstream file2_25("modif_sequences_3.txt", ios::in);//Open the file that contains the sequence of symbols provided in the seq_clustal_file
                                line6.clear();
                                if(file2_25)
                                    while(getline(file2_25,line6))//Read the one line sequence of symbols

                                        Symb=line6.at(t);
                                if(Symb.compare(s_asterix) !=0)//If codon has to be optimised
                                {
                                    selected_codon.clear();
                                    //Find the codons of rank 1 that code for the same amino acid
                                    for(int l=0; l<potential_rank.size(); l++)//For all potential codons
                                    {
                                        if(potential_rank.at(l)==1)//If the rank of the codon is 1
                                            selected_codon.push_back(potential_codon.at(l));//Subset that contains only the codons that can be corresponding codons. They are "equivalent" codons
                                    }

                                    //Select randomly the corresponding codon among the equivalent ones
                                    r = rand()%(int)(selected_codon.size());
                                    output_codon=selected_codon.at(r);

                                }
                                else //If the symbole for that codon is an asterix we mimic the speed of the native codon
                                {
                                    //Find the smallest distance to the native codon
                                    Delta=10000000.;//This will measure the distance of the potential ranks to the rank of the native codon
                                    for(int l=0; l<potential_rank.size(); l++)//For all potential codons
                                    {
                                        if(abs(potential_rank.at(l)-Rank2)<Delta)//Find the smallest distance to the native codon
                                            Delta = abs(potential_rank.at(l)-Rank2);
                                    }

                                    selected_codon.clear();
                                    //Find all codons among the potential ones whose rank is at this distance to the native codon
                                    for(int l=0; l<potential_rank.size(); l++)//For all potential codons
                                    {
                                        if(abs(potential_rank.at(l)-Rank2)==Delta)//If distance to the Rank is equal the the smallest distance (Delta)
                                            selected_codon.push_back(potential_codon.at(l));//Subset that contains only the codons that can be corresponding codons. They are "equivalent" codons.
                                    }

                                    //Select randomly the corresponding codon among the equivalent ones
                                    r = rand()%(int)(selected_codon.size());
                                    output_codon=selected_codon.at(r);
                                }
                                //Add it to the processed sequence
                                line3.append(output_codon);
                            }
                        }
                    }

                    ofstream file2_26("modif_sequences_6.txt",ios::app);
                    file2_26<<line3<<endl;
                }
            }
        }
    }
}


void Algo3()
{
//Core function of the mode 3 = "Optimisation and Conservation 2"
    ifstream file2_27("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host (the last one)
    if(file2_27)
    {
        i=0;
        line.clear();
        while(getline(file2_27, line))//Count the number of lines = number of natives +1 (host)
            i++;
    }

    if(i!=nbr_lines_in_block)//Nbr natives = nbr_lines_in_block-1, but here the host line is also counted
        output_debug = output_debug + "In STEP 3 STEP 4 and STEP 5 either the native organisms and/or the host have not been entered." + '\n'+ '\n';

    n=0;
    line.clear();
    ifstream file2_27_2("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_27_2, line))//Re-read the file that contains the name of all tables
    {
        n++;
        if(n==i)//When the last line is reached
            host_table = "Tables/Processed_"+line;//Store the name of the host table

    }

    /****** Make a loop on all natives to create the corresponding "0" and "S" file ("S" indicate slow codons) ******/
    test=2;//To flag the creation (only once) of the cpt_symbols vetcor
    i=0;
    line.clear();
    ifstream file2_27_3("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host
    while(getline(file2_27_3, line))//Re-read the file that contains the name of all tables
    {
        i++;
        ifstream file2_28("modif_sequences_5.txt", ios::in);//Open the aligned nucleotide sequences file
        if(file2_28)
        {
            n=0;
            line2.clear();
            while(getline(file2_28, line2))//For each sequence
            {
                if(test==2)//We make sure test can take this value only once
                {
                    test=0;
                    for(int l=0; l<line2.size()/(float)3.0; l++)//line2 is counted in nucleotides and we want one symbol per codon "0" (non slow) "S" (slow)
                        cpt_symbols.push_back(0.);
                }
                n++;
                input_codon.clear();
                line3.clear();
                if(i==n)//If the index of the table corresponds to the one of the sequence
                {
                    for (int t=0; t<line2.size()/(float)3.0; t++)//Read the sequence codon after codon
                    {
                        letter1 = line2.at(3*t);
                        letter2 = line2.at(3*t+1);
                        letter3 = line2.at(3*t+2);
                        input_codon = letter1+letter2+letter3;

                        if (input_codon.compare(triple_hyphen)!=0)//If not hyphens we search whether this is a slow codon or not
                        {
                            //Open the processed native table
                            ifstream file2_29("Tables/Processed_"+line, ios::in);//Search and store the amino acid and the rank of the native codon
                            line4.clear();
                            if(file2_29)
                            {
                                while(getline(file2_29,line4))
                                {
                                    istringstream in(line4);
                                    in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                    if(col3.compare(input_codon)==0)//When native codon is found
                                    {
                                        if(col8.compare(ch_S)==0)//If the codon is slow ("S")
                                            cpt_symbols.at(t)++;//Mark that one more native sequences shows a slow codon at that given position
                                    }
                                }
                            }
                            else
                                output_debug = output_debug + "In STEP 3 or STEP4: the table for the native:" + line + "could not be found."+ '\n' + "If you implemented this table yourself the format is not correct." + '\n' +"If this table is listed in the drop down menu of STEP 3, you might have erased it from the folder."+ '\n'+ '\n';

                        }
                    }
                }
            }
        }
    }

    /****** Analyse the conservation of slow codons for all native sequences and create a one line sequence of "0" and "S" to indicate where codon speed should be conserved instead of optimised ******/
    line3.clear();
    for(int l=0; l<cpt_symbols.size(); l++)
    {
        if(cpt_symbols.at(l)>= conservation_threshold*(i-1))//i is the number of lines in the Table_names.txt file, so i is equal to the number of natives +1 (for the host)
            line3.append("S");
        else
            line3.append("0");

    }
    //When the whole "counter" sequence has been translated into a sequence of "0" and "S" symbols
    ofstream file2_30("modif_sequences_7.txt",ios::app);
    file2_30<<line3<<endl;//Store it


    /****** In a similar fashion as in Algo 2, optimise all sequences but mimic native speed where slow codons are conserved ******/

    /****** Make a loop on all natives ******/
    i=0;
    line.clear();
    ifstream file2_31("Table_names.txt", ios::in);//Open the file that contains the name of all tables, natives and host (last one)
    while(getline(file2_31, line))//Re-read the file that contains the name of all tables
    {
        i++;
        ifstream file2_32("modif_sequences_5.txt", ios::in);//Open the aligned nucleotide sequences file
        if(file2_32)
        {
            n=0;
            line2.clear();
            while(getline(file2_32, line2))//For each sequence
            {
                n++;
                input_codon.clear();
                line3.clear();
                if(i==n)//If the index of the table corresponds to the one of the sequence
                {
                    for (int t=0; t<line2.size()/(float)3.0; t++)//Read the sequence codon after codon
                    {
                        letter1 = line2.at(3*t);
                        letter2 = line2.at(3*t+1);
                        letter3 = line2.at(3*t+2);
                        input_codon = letter1+letter2+letter3;

                        if (input_codon.compare(triple_hyphen)==0)//If there is a triple hyphen, we introduce it in the processed sequence
                            line3.append(triple_hyphen);

                        if (input_codon.compare(triple_hyphen)!=0)//If not, we search for corresponding codons
                        {
                            test=0;
                            //Open the processed native table
                            ifstream file2_33("Tables/Processed_"+line, ios::in);//Search and store the amino acid and the rank of the native codon
                            line4.clear();
                            if(file2_33)
                            {
                                amino_acid.clear();
                                while(getline(file2_33,line4))
                                {
                                    istringstream in(line4);
                                    in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                    if(col3.compare(input_codon)==0)//When native codon is found
                                    {
                                        Rank2 = col6;//Store its rank
                                        amino_acid = col1;
                                        test=1;
                                    }
                                }
                            }

                            if(test==0)//If the codon is not in the table it is a stop codon
                                line3.append(stop_codon);

                            //If the codon is not a stop codon
                            else//Find the corresponding codons that code for the same amino acid (either the optimal ones or the closest in rank)
                            {
                                ifstream file2_34(host_table, ios::in);//Open the host table
                                line6.clear();
                                if(file2_34)
                                {
                                    potential_codon.clear();
                                    potential_rank.clear();
                                    while(getline(file2_34,line6))//Read the host table
                                    {
                                        istringstream in(line6);
                                        in >> col1 >> col2 >> col3 >> col4 >> col5 >> col6 >> col7 >> col8;

                                        if(col1.compare(amino_acid)==0)//If the amino acid is the same as the one of the native codon
                                        {
                                            potential_codon.push_back(col3);//Store the codon
                                            potential_rank.push_back(col6);//Store the rank
                                        }

                                    }
                                }
                                else
                                    output_debug = output_debug + "In STEP 5 the host table could not be found. Make sure it is stored in the folder Tables/" + '\n'+ '\n';

                                //Check whether the codon speed has to be optimised or mimicked
                                ifstream file2_35("modif_sequences_7.txt", ios::in);//Open the file that contains the sequence of symbols "0" or "S" (if slow conserved codon)
                                line6.clear();
                                if(file2_35)
                                    while(getline(file2_35,line6))//Read the one line sequence of symbols

                                        Symb=line6.at(t);
                                if(Symb.compare(ch_0) ==0)//If codon has to be optimised
                                {
                                    selected_codon.clear();
                                    //Find the codons of rank 1 that code for the same amino acid
                                    for(int l=0; l<potential_rank.size(); l++) //For all potential codons
                                    {
                                        if(potential_rank.at(l)==1)//If the rank of the codon is 1
                                            selected_codon.push_back(potential_codon.at(l));//Subset that contains only the codons that can be corresponding codons. They are "equivalent" codons.
                                    }

                                    //Select randomly the corresponding codon among the equivalent ones
                                    r = rand()%(int)(selected_codon.size());
                                    output_codon=selected_codon.at(r);
                                }
                                else if(Symb.compare(ch_S) ==0)//If the symbole for that codon is an "S" we mimic the speed of the native codon
                                {

                                    //Find the smallest distance to the native codon
                                    Delta=10000000.;//This will measure the distance of the potential ranks to the rank of the native codon
                                    for(int l=0; l<potential_rank.size(); l++) //For all potential codons
                                    {
                                        if(abs(potential_rank.at(l)-Rank2)<Delta)//Find the smallest distance to the native codon
                                            Delta = abs(potential_rank.at(l)-Rank2);
                                    }

                                    selected_codon.clear();
                                    //Find all codons among the potential ones whose rank is at this distance to the native codon
                                    for(int l=0; l<potential_rank.size(); l++) //For all potential codons
                                    {
                                        if(abs(potential_rank.at(l)-Rank2)==Delta)//If distance to the Rank2 is equal the the smallest distance (Delta)
                                            selected_codon.push_back(potential_codon.at(l));//Subset that contains only the codons that can be corresponding codons. They are "equivalent" codons.
                                    }

                                    //Select randomly the corresponding codon among the equivalent ones
                                    r = rand()%(int)(selected_codon.size());
                                    output_codon=selected_codon.at(r);

                                }

                                //Add it to the processed sequence
                                line3.append(output_codon);
                            }
                        }
                    }

                    ofstream file2_36("modif_sequences_6.txt",ios::app);
                    file2_36<<line3<<endl;
                }
            }
        }
    }
}


void clear_output_sequences(string para_mode)
{
//Remove hyphens and replace U by T in the output nucleotide sequences
    ifstream file2_36("modif_sequences_6.txt", ios::in);//Open the file that contains the aligned processed nucleotide sequences

    if(file2_36)
    {
        line.clear();
        while(getline(file2_36, line))//Read it line by line
        {
            line2.clear();
            for (int t=0; t<line.size(); t++)//For each character
            {
                letter1=line.at(t);
                if(letter1.compare(ch_U)==0)//If it is a U
                    line2.append("T");//Store a T
                else if(letter1.compare(hyphen) !=0)//For other characters that are not hyphen
                    line2 = line2 + letter1;//Store them
            }

            ofstream file2_37(para_mode + ".txt", ios::app);//Print the output final processed sequence
            file2_37<<line2<<endl;
        }
    }
}

void compare_sequences(string para_mode)
{
//Determine how similar to the native sequence is each output sequence
    ifstream file2_38("modif_sequences_4.txt", ios::in);//Open the file that contains the input nucleotide sequences
    i = 0;
    if(file2_38)
    {
        line.clear();
        while(getline(file2_38, line))//Read it line by line
        {
            ifstream file2_39(para_mode + ".txt", ios::in);//Open the file that contains the processed nucleotide sequences
            n = 0;
            if(file2_39)
            {
                line2.clear();
                while(getline(file2_39, line2))//Read it line by line
                {
                    if (i == n)
                    {
                        letter1.clear();
                        letter2.clear();
                        letter3.clear();
                        input_codon.clear();
                        counter_similarity=0;
                        for (int t=0; t<line.size()/(float)3.0; t++)//At each codon
                        {
                            letter1 = line.at(3*t);
                            letter2 = line.at(3*t+1);
                            letter3 = line.at(3*t+2);
                            if(letter1.compare(ch_U)==0)
                                letter1 = ch_T;
                            if(letter2.compare(ch_U)==0)
                                letter2 = ch_T;
                            if(letter3.compare(ch_U)==0)
                                letter3 = ch_T;
                            input_codon = letter1+letter2+letter3;
                            letter1.clear();
                            letter2.clear();
                            letter3.clear();
                            output_codon.clear();
                            letter1 = line2.at(3*t);
                            letter2 = line2.at(3*t+1);
                            letter3 = line2.at(3*t+2);
                            output_codon = letter1+letter2+letter3;

                            if(input_codon.compare(output_codon)==0)//If native and processed codons are similar
                                counter_similarity++;//Count the similarity
                        }
                    }
                    n++;
                }
            }

            ofstream file2_40(para_mode + "_identity_percentage.txt", ios::app);//Print the file that contains the percentage of similarity for each sequence after processing
            file2_40<<"Sequence "<<i+1<<" is "<<100*((float)(counter_similarity)/(float)(line.size()/(float)3.0))<<" percent identical to its input sequence, when comparing each codon."<<endl;//Print "i+1" in the file since counting in the code is from "0", which could be misleading for the user.
            i++;
        }
    }
}


string Process_funct(string para_mode)
{
    srand (time(NULL));


    ofstream file2_2("modif_sequences_1.txt", ios::out);//Stores amino acids sequences without blanck line between blocks and with clean line ends

    ofstream file2_5("modif_sequences_2.txt", ios::out);//Stores amino acids sequences, one sequence per line without symbol line

    ofstream file2_7("modif_sequences_3.txt", ios::out);//Stores the complete symbol sequence on one line (from Clustal file)

    ofstream file2_9("modif_sequences_4.txt", ios::out);//Stores nulceotide sequences concatenated, cleared from "T" letters and from final stop codon

    ofstream file2_13("modif_sequences_5.txt", ios::out);//Stores aligned nucleotide sequences

    ofstream file2_19("modif_sequences_6.txt", ios::out);//Stores aligned processed nucleotide sequences

    ofstream file2_14("modif_sequences_7.txt", ios::out);//Stores the complete sequence of "0" and "S" symbols for the conservation of slow codons on one line

    ofstream file2_22(para_mode+".txt", ios::out);//Stores the final processed de-aligned and T based (instead of U) sequences of nucleotides

    ofstream file2_2_41(para_mode + "_identity_percentage.txt", ios::out);//Stores the percentage of similarity of each sequence (comparison before vs after processing)

    Para_mode = Para_mode+para_mode;

    output_debug.clear();


    find_files();

    if(para_mode.compare("Direct_mapping") != 0)
        seq_clustal_formating();

    clear_nucleotide_sequences();

    if(para_mode.compare("Direct_mapping") != 0)
        alignement_seq_nucleo();

    if(para_mode.compare("Direct_mapping") == 0)
        Algo1();
    else if(para_mode.compare("Optimisation_and_conservation_1") == 0)
        Algo2();
    else if(para_mode.compare("Optimisation_and_conservation_2") == 0)
        Algo3();

    clear_output_sequences(para_mode);
    compare_sequences(para_mode);

    return output_debug;
}

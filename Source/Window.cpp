#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <time.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <string.h>
#include <list>
#include <math.h>
#include <gtk/gtk.h>
#include <cstring>
#include <errno.h>

using namespace std;
using std::ifstream;
using std::ofstream;
using std::ios;

char empty_c[] = "EMPTY";
char true_empty_c[] = "";
char else_c[] = "ELSE";
const char *input_text;
const char *input_text2;
const char *current_entry;
string table;
string output_text;
string end_line ("\n");
string output_file;
string Host;
string Mode;

ofstream file1("Input_file_names.txt",ios::out);
ofstream file2("Input_file_names.txt",ios::app);
ofstream file3("Table_names.txt",ios::out);
ofstream file4("Table_names.txt",ios::app);



GtkWidget *window;
GtkWidget *dropdown_list1;
GtkWidget *dropdown_list2;
GtkWidget *dropdown_list3;
GtkWidget *dropdown_list4;
GtkWidget *dropdown_list5;
GtkWidget *dropdown_list6;
GtkWidget *dropdown_list7;
GtkWidget *dropdown_list8;
GtkWidget *dropdown_list9;
GtkWidget *dropdown_list10;
GtkWidget *parentbox;
GtkWidget *box1;
GtkWidget *box2;
GtkWidget *box3;
GtkWidget *box3_1;
GtkWidget *box3_2;
GtkWidget *box3_3;
GtkWidget *box3_4;
GtkWidget *box3_5;
GtkWidget *box4;
GtkWidget *box5;
GtkWidget *buttonHost1;
GtkWidget *buttonHost2;
GtkWidget *buttonHost3;
GtkWidget *buttonHost4;
GtkWidget *buttonMode1;
GtkWidget *buttonMode2;
GtkWidget *buttonMode3;
GtkWidget *buttonRun;
GtkWidget *label_1;
GtkWidget *label_2;
GtkWidget *label_3_1;
GtkWidget *label_3_2;
GtkWidget *label_3_3;
GtkWidget *label_3_4;
GtkWidget *label_3_5;
GtkWidget *label_4;
GtkWidget *label_5;
GtkWidget *label;
GtkWidget *entry1;
GtkWidget *entry2;
GtkWidget *entry3_2_1;
GtkWidget *entry3_2_2;
GtkWidget *entry3_2_3;
GtkWidget *entry3_2_4;
GtkWidget *entry3_2_5;
GtkWidget *entry3_2_6;
GtkWidget *entry3_2_7;
GtkWidget *entry3_2_8;
GtkWidget *entry3_2_9;
GtkWidget *entry3_2_10;
GtkWidget *output_1;
GtkWidget *output_2;
GtkWidget *view;
GtkTextBuffer *buffer_1;
GtkTextBuffer *buffer_2;

/*******/
GtkWidget *instructions;
GtkWidget *scrolledwindow;
GtkWidget *label_instructions;
const char *text_instructions;
gchar* converted_instructions;

/*******/
string output_remove;

GtkWidget *debugging;
GtkWidget *scrolledwindow_debug;
GtkWidget *label_debug;
string output_debug_str;
gchar* converted_debug;

/*******/
gchar* converted_output_1;
gchar* converted_output_2;


string Process_funct(string para_mode);
void Process_tables();

//Append all lines to the combo_box_text dropdown_list
void make_dropdown_list(GtkComboBoxText *dropdown_list)
{
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "EMPTY");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "EMPTY");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Arabidopsis_thaliana");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Bacillus_subtilis");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Caenorhabditis_elegans");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Danio_rerio");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Drosophila_melanogaster");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Escherichia_coli");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Gallus_gallus");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Homo_sapiens");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Komagataella_pastoris");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Methanocaldococcus_jannaschii");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Saccharomyces_cerevisiae");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Staphylococcus_aureus");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "Xenopus_tropicalis");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(dropdown_list), NULL, "ELSE");
    gtk_combo_box_set_active(GTK_COMBO_BOX(dropdown_list), 1);

}
//Turn bold the label of the host selected, make sure other hosts label are not bold, set Host
void E_coli_host()
{

    label = gtk_bin_get_child(GTK_BIN(buttonHost1));//Call label the only child of the button "buttonHost1"
    gtk_label_set_markup(GTK_LABEL(label),"<b>Escherichia coli</b>");//Markes the label label in bold
    label = gtk_bin_get_child(GTK_BIN(buttonHost2));
    gtk_label_set_markup(GTK_LABEL(label), "Saccharomyces cerevisiae");
    label = gtk_bin_get_child(GTK_BIN(buttonHost3));
    gtk_label_set_markup(GTK_LABEL(label), "Komagataella pastoris");
    label = gtk_bin_get_child(GTK_BIN(buttonHost4));
    gtk_label_set_markup(GTK_LABEL(label), "Bacillus subtilis");

    Host.clear();
    Host = ("Escherichia_coli");
}
//Turn bold the label of the host selected, make sure other hosts label are not bold, set Host
void S_cerevisiae_host()
{

    label = gtk_bin_get_child(GTK_BIN(buttonHost1));
    gtk_label_set_markup(GTK_LABEL(label),"Escherichia coli");
    label = gtk_bin_get_child(GTK_BIN(buttonHost2));
    gtk_label_set_markup(GTK_LABEL(label), "<b>Saccharomyces cerevisiae</b>");
    label = gtk_bin_get_child(GTK_BIN(buttonHost3));
    gtk_label_set_markup(GTK_LABEL(label), "Komagataella pastoris");
    label = gtk_bin_get_child(GTK_BIN(buttonHost4));
    gtk_label_set_markup(GTK_LABEL(label), "Bacillus subtilis");

    Host.clear();
    Host = ("Saccharomyces_cerevisiae");
}
//Turn bold the label of the host selected, make sure other hosts label are not bold, set Host
void P_pastoris_host()
{

    label = gtk_bin_get_child(GTK_BIN(buttonHost1));
    gtk_label_set_markup(GTK_LABEL(label),"Escherichia coli");
    label = gtk_bin_get_child(GTK_BIN(buttonHost2));
    gtk_label_set_markup(GTK_LABEL(label), "Saccharomyces cerevisiae");
    label = gtk_bin_get_child(GTK_BIN(buttonHost3));
    gtk_label_set_markup(GTK_LABEL(label), "<b>Komagataella pastoris</b>");
    label = gtk_bin_get_child(GTK_BIN(buttonHost4));
    gtk_label_set_markup(GTK_LABEL(label), "Bacillus subtilis");

    Host.clear();
    Host = ("Komagataella_pastoris");
}
//Turn bold the label of the host selected, make sure other hosts label are not bold, set Host
void B_subtilis_host()
{

    label = gtk_bin_get_child(GTK_BIN(buttonHost1));
    gtk_label_set_markup(GTK_LABEL(label),"Escherichia coli");
    label = gtk_bin_get_child(GTK_BIN(buttonHost2));
    gtk_label_set_markup(GTK_LABEL(label), "Saccharomyces cerevisiae");
    label = gtk_bin_get_child(GTK_BIN(buttonHost3));
    gtk_label_set_markup(GTK_LABEL(label), "Komagataella pastoris");
    label = gtk_bin_get_child(GTK_BIN(buttonHost4));
    gtk_label_set_markup(GTK_LABEL(label), "<b>Bacillus subtilis</b>");

    Host.clear();
    Host = ("Bacillus_subtilis");
}
//Turn bold the label of the mode selected, make sure other modes label is not bold, set Mode
void Mode1_selected()
{

    label = gtk_bin_get_child(GTK_BIN(buttonMode1));
    gtk_label_set_markup(GTK_LABEL(label),"<b>Direct mapping</b>");
    label = gtk_bin_get_child(GTK_BIN(buttonMode2));
    gtk_label_set_markup(GTK_LABEL(label),"Optimisation and conservation 1");
    label = gtk_bin_get_child(GTK_BIN(buttonMode3));
    gtk_label_set_markup(GTK_LABEL(label),"Optimisation and conservation 2");

    Mode.clear();
    Mode = ("Direct_mapping");
}
//Turn bold the label of the mode selected, make sure other modes label is not bold, set Mode
void Mode2_selected()
{

    label = gtk_bin_get_child(GTK_BIN(buttonMode1));
    gtk_label_set_markup(GTK_LABEL(label),"Direct mapping");
    label = gtk_bin_get_child(GTK_BIN(buttonMode2));
    gtk_label_set_markup(GTK_LABEL(label),"<b>Optimisation and conservation 1</b>");
    label = gtk_bin_get_child(GTK_BIN(buttonMode3));
    gtk_label_set_markup(GTK_LABEL(label),"Optimisation and conservation 2");

    Mode.clear();
    Mode = ("Optimisation_and_conservation_1");
}
//Turn bold the label of the mode selected, make sure other modes label is not bold, set Mode
void Mode3_selected()
{

    label = gtk_bin_get_child(GTK_BIN(buttonMode1));
    gtk_label_set_markup(GTK_LABEL(label),"Direct mapping");
    label = gtk_bin_get_child(GTK_BIN(buttonMode2));
    gtk_label_set_markup(GTK_LABEL(label),"Optimisation and conservation 1");
    label = gtk_bin_get_child(GTK_BIN(buttonMode3));
    gtk_label_set_markup(GTK_LABEL(label),"<b>Optimisation and conservation 2</b>");

    Mode.clear();
    Mode = ("Optimisation_and_conservation_2");
}

//Create three output files based on the currently active string in ComboBoxText dropdown
void Make_table_files(GtkComboBoxText *dropdown, GtkEntry *entry)
{

    //Collect and convert to char * the currently active string in ComboBoxText dropdown
    current_entry=(char*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(dropdown));
    input_text=(char*)gtk_entry_get_text(GTK_ENTRY(entry));//Collect the name of the native microorganism input file and store it in file4

    if((strcmp(else_c,current_entry)==0) && (strcmp(true_empty_c,input_text)==0)) //If current_entry is "ELSE" and no alternative file name has been entered in STEP 4
        output_debug_str = output_debug_str + "In STEP 3 ELSE is selected but on the same line in STEP 4 no alternative table file name is entered. Enter one." + '\n'+ '\n';


    if((strcmp(else_c,current_entry)==0) && (strcmp(true_empty_c,input_text)!=0))//If current_entry is "ELSE" and an alternative file name has been entered in STEP 4
    {
        //Fill file4 with correpsonding file name
        table.clear();
        table.append(input_text);
        file4 <<table<<endl;
      //  free((char*)input_text);//Clear the variable input_text
    }

    if((strcmp(empty_c,current_entry)!=0) && (strcmp(else_c,current_entry)!=0) && (strcmp(true_empty_c,input_text)==0))//If not "EMPTY" and not "ELSE" and no alternative file name has been entered in STEP4
    {
        //Fill file4 with correpsonding file name
        table.clear();
        table.append(current_entry);
        table.append(".txt");
        file4 <<table<<endl;
    }

    if((strcmp(empty_c,current_entry)!=0) && (strcmp(else_c,current_entry)!=0) && (strcmp(true_empty_c,input_text)!=0))
            output_debug_str = output_debug_str + "In STEP 3 a table is selected and on the same line in STEP 4 an alternative table file is entered. Enter only one of them." + '\n'+ '\n';




    if((strcmp(empty_c,current_entry)==0) && (strcmp(true_empty_c,input_text)!=0))
            output_debug_str = output_debug_str + "In STEP 3 EMPTY is selected and on the same line in STEP 4 an alternative table file is entered. Select ELSE instead of EMPTY." + '\n'+ '\n';


    if(strcmp(true_empty_c,input_text)!=0)
        free((char*)input_text);//Clear the variable input_text

}

void Run_process()
{

    output_debug_str.clear();

    label = gtk_bin_get_child(GTK_BIN(buttonRun));//Call label the only child of the button "buttonRun"
    gtk_label_set_markup(GTK_LABEL(label),"Run started!");//Change the label
    //Call Make_table_files function for each possible native microorganism (up to 10)
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list1),GTK_ENTRY(entry3_2_1));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list2),GTK_ENTRY(entry3_2_2));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list3),GTK_ENTRY(entry3_2_3));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list4),GTK_ENTRY(entry3_2_4));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list5),GTK_ENTRY(entry3_2_5));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list6),GTK_ENTRY(entry3_2_6));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list7),GTK_ENTRY(entry3_2_7));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list8),GTK_ENTRY(entry3_2_8));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list9),GTK_ENTRY(entry3_2_9));
    Make_table_files(GTK_COMBO_BOX_TEXT(dropdown_list10),GTK_ENTRY(entry3_2_10));

    //Add on the last line of file4, the file name of the host micro-organism, its table will be processed too
    table.clear();
    table.append(Host);
    table.append(".txt");
    file4 <<table<<endl;


    input_text=(char*)gtk_entry_get_text(GTK_ENTRY(entry1));//Collect the name of the clustal alignment input file and store it in file2
    input_text2=(char*)gtk_entry_get_text(GTK_ENTRY(entry2));//Collect the name of the nucleotide sequences input file and store it in file2

    //Debugging tests
    if((strcmp(true_empty_c,input_text)==0) && (strcmp(true_empty_c,input_text2)==0))
        output_debug_str = output_debug_str + "In STEP 1 and STEP 2 the file name has not been entered." + '\n'+ '\n';

    if((strcmp(true_empty_c,input_text)!=0) && (strcmp(true_empty_c,input_text2)==0))
        output_debug_str = output_debug_str + "In STEP 2 the file name has not been entered." + '\n'+ '\n';

    if((strcmp(true_empty_c,input_text)==0) && (Mode.compare("Direct_mapping")!=0) && (Mode.compare("")!=0))
        output_debug_str = output_debug_str + "In STEP 1 the file name has not been entered." + '\n'+ '\n';

    if((strcmp(true_empty_c,input_text)!=0) && (Mode.compare("Direct_mapping")==0))
        output_debug_str = output_debug_str + "In STEP 1 the file name is not necessary with Direct mapping mode. Leave this field empty." + '\n'+ '\n';

    if(Host.compare("")==0)
        output_debug_str = output_debug_str + "In STEP 5 the host has not been selected. Select one." + '\n'+ '\n';

    if(Mode.compare("")==0)
        output_debug_str = output_debug_str + "In STEP 6 the mode has not been selected. Select one." + '\n'+ '\n';

    if(output_debug_str.size()==0)
    {
        if(strcmp(true_empty_c,input_text)!=0)
            {
             file2<<input_text<<endl;
             free((char*)input_text);//Clear the variable input_text
            }

        file2<<input_text2<<endl;
        free((char*)input_text2);//Clear the variable input_text

    Process_tables();//Run the code that processes the modification of the raw natives and host tables to be used by the "Process_funct" code
    output_debug_str = output_debug_str + Process_funct(Mode);//Run the code that processes the modification of the sequences and concatenate debugging messages

    /*******************Cleaning**************************/
    remove("modif_sequences_1.txt");
    remove("modif_sequences_2.txt");
    remove("modif_sequences_3.txt");
    remove("modif_sequences_4.txt");
    remove("modif_sequences_5.txt");
    remove("modif_sequences_6.txt");
    remove("modif_sequences_7.txt");
    remove("Input_file_names.txt");
    remove("Table_names.txt");
    system("rm Tables/Processed_*");
    }

    if(output_debug_str.size()>0)//If a bug has been detected
    {
        /*******************Debugging window**************************/

    //Create debugging window
    debugging = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(debugging),"Debugging report");
    gtk_window_set_resizable(GTK_WINDOW(debugging),TRUE);
    gtk_window_set_default_size(GTK_WINDOW(debugging),600,200);

    output_debug_str = output_debug_str + "RESTART THE SOFTWARE AND REFER TO THE INSTRUCTIONS WINDOW.";

    //char output_debug_ch[output_debug_str.size()+1];
    char* output_debug_ch = new char[output_debug_str.size() + 1];
    output_debug_str.copy(output_debug_ch, output_debug_str.size() +1);
    output_debug_ch[output_debug_str.size()] = '\0';

    converted_debug = g_locale_to_utf8(output_debug_ch, -1, NULL, NULL, NULL);//Convert the gchar to a valid UTF8

    label_debug=gtk_label_new(converted_debug);//Apply the conversion to label_instructions
    gtk_label_set_xalign(GTK_LABEL(label_debug),0.0);//Set the x position of label_instructions
    gtk_label_set_yalign(GTK_LABEL(label_debug),0.0);//Set the y position of label_instructions
    gtk_label_set_line_wrap(GTK_LABEL(label_debug),TRUE);//Make lines of the label wrapped when window too short
    g_free(converted_debug);//Free the memory

    gtk_label_set_use_markup(GTK_LABEL(label_debug), TRUE);//Label_instructions text marked with Pango text markup
    scrolledwindow_debug = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(scrolledwindow_debug), label_debug);
    gtk_container_add(GTK_CONTAINER(debugging), scrolledwindow_debug);

    gtk_widget_show_all(debugging);//Show all widgets packed in window


        /**************************Cleaning**********************************/

        //Remove the output files that may have been generated
        output_remove = Mode+".txt";
       // char output_remove_ch_1[output_remove.size() +1];
        char* output_remove_ch_1 = new char[output_remove.size() + 1];
        output_remove.copy(output_remove_ch_1, output_remove.size() +1);
        output_remove_ch_1[output_remove.size()] = '\0';
        remove(output_remove_ch_1);

        output_remove.clear();
        output_remove = Mode+"_identity_percentage.txt";
       // char output_remove_ch_2[output_remove.size() +1];
        char* output_remove_ch_2 = new char[output_remove.size() + 1];
        output_remove.copy(output_remove_ch_2, output_remove.size() +1);
        output_remove_ch_2[output_remove.size()] = '\0';
        remove(output_remove_ch_2);

    }

    else //If the debug report is empty
    {
        //The name of the output file to be loaded on the view output depends on the Mode selected
        output_file.clear();
        output_file = Mode;
        output_file.append(".txt");

        buffer_1 = gtk_text_view_get_buffer(GTK_TEXT_VIEW(output_1));//Buffer_1 is the buffer to be displayed in the view output

        //char output_ch_1[output_file.size()+1];
        char* output_ch_1 = new char[output_file.size() + 1];
        output_file.copy(output_ch_1, output_file.size() +1);
        output_ch_1[output_file.size()] = '\0';
        converted_output_1 = g_locale_to_utf8(output_ch_1, -1, NULL, NULL, NULL);//Convert the gchar to a valid UTF8
        gtk_text_buffer_set_text(buffer_1, converted_output_1, -1);//Convert output_file to const gchar and set buffer_1 to output_file converted

        output_file.clear();
        output_file = Mode;
        output_file.append("_identity_percentage.txt");

        buffer_2 = gtk_text_view_get_buffer(GTK_TEXT_VIEW(output_2));//Buffer_2 is the buffer to be displayed in the view output

        //char output_ch_2[output_file.size()+1];
        char* output_ch_2 = new char[output_file.size() + 1];
        output_file.copy(output_ch_2, output_file.size() +1);
        output_ch_2[output_file.size()] = '\0';
        converted_output_2 = g_locale_to_utf8(output_ch_2, -1, NULL, NULL, NULL);//Convert the gchar to a valid UTF8
        gtk_text_buffer_set_text(buffer_2, converted_output_2, -1);//Convert output_file to const gchar and set buffer_2 to output_file converted
    }


}

void Instructions_Window()
{
    //Create instructions window
    instructions = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(instructions),"Software instructions");
    gtk_window_set_resizable(GTK_WINDOW(instructions),TRUE);
    gtk_window_set_default_size(GTK_WINDOW(instructions),800,400);

    text_instructions = "<span face=\"Verdana\" foreground=\"#39b500\" ><b>ExpressInHost: A codon tuning tool for the expression of recombinant proteins in host microorganisms</b></span>\n"
                        "\n"
                        "\n"
                        "<b>WARNINGS</b>\n"
                        "<b>For an efficient use of this software, reading at first this complete list of simple instructions is a must.</b> \n"
                        "\n"
                        "<b>The input files (step 1 and step 2) and the folder Tables/ must be located in the same folder as the software.</b> \n"
                        "\n"
                        "<b>All input files must be text files with extension .txt. To avoid any confusion, WINDOWS users are highly recommanded to turn on 'show file extensions' that are hidden by default.</b> \n"
                        "\n"
                        "\n"
                        "<b>STEP 1</b>\n"
                        "The <b>Clustal alignment input file</b> is not necessary if you plan to use the mode <b>Direct mapping</b> (at step 6). "
                        "Then, you must leave this field empty. Instead, if at step 6 you plan to use the mode <b>Optimisation and conservation 1</b> or <b>Optimisation and conservation 2 </b>, "
                        "the <b>Clustal alignment input file</b> is a text file of Clustal format that contains the target "
                        "sequence and the orthologous sequences. The latter are necessary to determine the points of speed conservation (see step 6). "
                        "The format of the <b>Clustal alignment input file</b> is crucial, thus consider seeing the <b>Example files</b>. "
                        "The target sequence can have any rank in the Clustal alignment, but it must be the same as in the <b>nucleotide sequences input file</b> (step 2). \n"
                        "\n"
                        "The name of the <b>Clustal alignment input file</b> must be free from white spaces and special "
                        "characters, and its extension must be <b>.txt</b>, example: My_input_file.txt \n"
                        "\n"
                        "\n"
                        "<b>STEP 2</b>\n"
                        "The <b>nucleotide sequences input file</b> must be a text file of FASTA format. "
                        "<b>Two essential format modifications must be performed on it:</b> "
                        "<b>i) the sequences must not be separated by empty lines (like default FASTA format), but instead by the symbol #, ii) after the very last sequence an # should also be placed, BUT no empty line after it.</b> "
                        "The format of the <b>nucleotide sequences input file</b> is crucial, thus consider seeing the <b>Example files</b>. "
                        "These sequences are those corresponding to the amino acid sequences aligned in the <b>clustal "
                        "alignment input file</b> (step 1), and must strictly follow the same order as in that file. The sequences "
                        "may or may not end with one of the stop codons: UAA, UAG and UGA. The sequences may contain stop codons within the "
                        "sequences only for the <b>Direct mapping</b> mode (step 6). \n"
                        "\n"
                        "The name of the <b>nucleotide sequences input file</b> must be free from white spaces and special "
                        "characters, and its extension must be <b>.txt</b>, example: Another_input_file.txt \n"
                        "\n"
                        "\n"
                        "<b>STEP 3</b>\n"
                        "The <b>native organisms</b> are the organisms in which the sequences aligned in the input files "
                        "(step 1 and step 2) are naturally expressed. If a native organism is not available in the drop "
                        "down list, select the last item: <b>ELSE</b>. The order of the <b>list of native organisms</b> must strictly "
                        "follow the same order as in the <b>Clustal alignment input file</b> "
                        "(step 1), and in the <b>nucleotide sequences input file</b> (step 2). \n"
                        "\n"
                        "\n"
                        "<b>STEP 4</b>\n"
                        "If the native organism is available in the drop down list in step 3, the corresponding field "
                        "must be left empty. Otherwise, indicate in the corresponding field the name of the <b>input table file</b> "
                        "that you created for that organism. \n"
                        "\n"
                        "The name of the <b>input table file</b> must be free from white spaces and special "
                        "characters, and its extension must be <b>.txt</b>, example: My_table.txt \n"
                        "\n"
                        "\n"
                        "<b>STEP 5</b>\n"
                        "Select the microorganism in which the target gene should be expressed. \n"
                        "\n"
                        "\n"
                        "<b>STEP 6</b>\n"
                        "The <b>Direct mapping</b> mode mimicks, for each sequence, the translation speed profile from the native "
                        "organism into the selected host. The <b>Optimisation and conservation 1</b> mode maximises, for each "
                        "sequence, the translation speed for each codon excepted those where amino acids are highly conserved "
                        "among the orthologous sequences. The <b>Optimisation and conservation 2</b> mode maximises, for each "
                        "sequence, the translation speed for each codon excepted those where slow translation speed is highly "
                        "conserved among the orthologous sequences. \n"
                        "\n"
                        "\n"
                        "<b>STEP 7</b>\n"
                        "Runs the tuning of the sequences. \n"
                        "\n"
                        "\n"
                        "<b>STEP 8</b>\n"
                        "When the sequences are tuned, the name of the output file containing the tuned nucleotide sequences is "
                        "displayed in that field. In the output file, each sequence is on a different line, ordered in the same order "
                        "as in the input files of step 1 and step 2. \n"
                        "\n"
                        "\n"
                        "<b>STEP 9</b>\n"
                        "When the sequences are tuned, the name of the output file containing the percentage of similarity between the native and the "
                        "tuned sequences is displayed in that field. In the output file, the percentage of similarity for each sequence "
                        "is on a different line, ordered in the same order as in the input files of step 1 and step 2 \n"
                        "\n"
                        "\n"
                        "<b>TO USE THE SOFTWARE ANEW, FIRST CLOSE IT AND RESTART IT.</b>\n";


    converted_instructions = g_locale_to_utf8(text_instructions, -1, NULL, NULL, NULL);//Convert the gchar to a valid UTF8

    label_instructions=gtk_label_new(converted_instructions);//Apply the conversion to label_instructions
    gtk_label_set_xalign(GTK_LABEL(label_instructions),0.0);//Set the x position of label_instructions
    gtk_label_set_yalign(GTK_LABEL(label_instructions),0.0);//Set the y position of label_instructions
    gtk_label_set_line_wrap(GTK_LABEL(label_instructions),TRUE);//Make lines of the label wrapped when window too short
    g_free(converted_instructions);//Free the memory

    gtk_label_set_use_markup(GTK_LABEL(label_instructions), TRUE);//Label_instructions text marked with Pango text markup
    scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(scrolledwindow), label_instructions);
    gtk_container_add(GTK_CONTAINER(instructions), scrolledwindow);

    gtk_widget_show_all(instructions);//Show all widgets packed in window
}


int main(int argc, char **argv)
{


    /*******************Main GUI window**************************/


    gtk_init (&argc,&argv);//Initiate the application
    //Create new window and features
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window),"Software ExpressInHost");
    gtk_window_set_resizable(GTK_WINDOW(window),TRUE);
    gtk_window_set_default_size(GTK_WINDOW(window),1000,600);


    //Create ComboBoxText to contain the list of possible native microorganisms (up to 10)
    dropdown_list1=gtk_combo_box_text_new();
    dropdown_list2=gtk_combo_box_text_new();
    dropdown_list3=gtk_combo_box_text_new();
    dropdown_list4=gtk_combo_box_text_new();
    dropdown_list5=gtk_combo_box_text_new();
    dropdown_list6=gtk_combo_box_text_new();
    dropdown_list7=gtk_combo_box_text_new();
    dropdown_list8=gtk_combo_box_text_new();
    dropdown_list9=gtk_combo_box_text_new();
    dropdown_list10=gtk_combo_box_text_new();


    parentbox = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);//Create main container that sorts children vertically

    box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(parentbox), box1);//Add box1 in parentbox
    label_1 = gtk_label_new("");//Create the empty label label_1
    gtk_label_set_markup(GTK_LABEL(label_1), "<b>1. Name of clustal alignment input file (without white space, with .txt extension, ex: simple_file.txt)</b>");//Fill label_1 with bold text
    gtk_box_pack_start(GTK_BOX(box1),label_1, TRUE, TRUE,10);//Add label_1 to box1, expand TRUE, fill TRUE, padding 10
    entry1 = gtk_entry_new();//Create a new entry widget
    gtk_box_pack_start(GTK_BOX(box1),entry1, TRUE, TRUE,10);


    box2 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(parentbox), box2);
    label_2 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_2), "<b>2. Name of nucleotide sequences input file (without white space, with .txt extension, ex: another_file.txt)</b>");
    gtk_box_pack_start(GTK_BOX(box2),label_2, TRUE, TRUE,10);
    entry2 = gtk_entry_new();
    gtk_widget_set_valign(entry2, GTK_ALIGN_FILL);
    gtk_box_pack_start(GTK_BOX(box2),entry2, TRUE, TRUE,10);


    box3 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,10);//Create box3, a new box, that sortes children horizontally
    gtk_widget_set_hexpand(box3, TRUE);//Box3 expands horizontally
    gtk_container_add(GTK_CONTAINER(parentbox), box3);

    box3_1 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_widget_set_hexpand(box3_1, TRUE);
    gtk_container_add(GTK_CONTAINER(box3), box3_1);
    label_3_1 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_3_1), "<b>3. List of native microorganisms</b>");
    gtk_box_pack_start(GTK_BOX(box3_1),label_3_1, TRUE, TRUE,10);
    //Fill each dropdown_list by calling make_dropdown_list function
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list1));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list2));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list3));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list4));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list5));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list6));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list7));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list8));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list9));
    make_dropdown_list(GTK_COMBO_BOX_TEXT(dropdown_list10));
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list1);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list2);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list3);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list4);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list5);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list6);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list7);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list8);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list9);
    gtk_container_add(GTK_CONTAINER(box3_1), dropdown_list10);




    box3_2 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_widget_set_hexpand(box3_2, TRUE);
    gtk_container_add(GTK_CONTAINER(box3), box3_2);
    label_3_2 = gtk_label_new("");//Create the empty label label_3_2
    gtk_label_set_markup(GTK_LABEL(label_3_2), "<b>4. Name of table file if native is 'ELSE'</b>");//Fill label_3_2 with bold text
    gtk_box_pack_start(GTK_BOX(box3_2),label_3_2, TRUE, TRUE,10);//Add label_1 to box1, expand TRUE, fill TRUE, padding 10
    entry3_2_1 = gtk_entry_new();//Create a new entry widget
    entry3_2_2 = gtk_entry_new();//Create a new entry widget
    entry3_2_3 = gtk_entry_new();//Create a new entry widget
    entry3_2_4 = gtk_entry_new();//Create a new entry widget
    entry3_2_5 = gtk_entry_new();//Create a new entry widget
    entry3_2_6 = gtk_entry_new();//Create a new entry widget
    entry3_2_7 = gtk_entry_new();//Create a new entry widget
    entry3_2_8 = gtk_entry_new();//Create a new entry widget
    entry3_2_9 = gtk_entry_new();//Create a new entry widget
    entry3_2_10 = gtk_entry_new();//Create a new entry widget
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_1);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_2);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_3);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_4);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_5);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_6);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_7);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_8);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_9);
    gtk_container_add(GTK_CONTAINER(box3_2), entry3_2_10);




    box3_3 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_widget_set_hexpand(box3_3, TRUE);
    gtk_widget_set_valign(box3_3, GTK_ALIGN_START);//Make the content of box3_2 align at the top of its container box
    gtk_container_add(GTK_CONTAINER(box3), box3_3);
    label_3_3 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_3_3), "<b>5. Host microorganism</b>");
    gtk_box_pack_start(GTK_BOX(box3_3),label_3_3, TRUE, TRUE,10);
    //Create new button with label
    buttonHost1 = gtk_button_new_with_label("Escherichia coli");
    buttonHost2 = gtk_button_new_with_label("Saccharomyces cerevisiae");
    buttonHost3 = gtk_button_new_with_label("Komagataella pastoris");
    buttonHost4 = gtk_button_new_with_label("Bacillus subtilis");
    //When button buttonHostX is clicked call callback function E_coli_host, NULL = no data passed to the function
    g_signal_connect(buttonHost1, "clicked", G_CALLBACK(E_coli_host), NULL);
    g_signal_connect(buttonHost2, "clicked", G_CALLBACK(S_cerevisiae_host), NULL);
    g_signal_connect(buttonHost3, "clicked", G_CALLBACK(P_pastoris_host), NULL);
    g_signal_connect(buttonHost4, "clicked", G_CALLBACK(B_subtilis_host), NULL);

    gtk_container_add(GTK_CONTAINER(box3_3), buttonHost1);
    gtk_container_add(GTK_CONTAINER(box3_3), buttonHost2);
    gtk_container_add(GTK_CONTAINER(box3_3), buttonHost3);
    gtk_container_add(GTK_CONTAINER(box3_3), buttonHost4);


    box3_4 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_widget_set_hexpand(box3_4, TRUE);
    gtk_widget_set_valign (box3_4, GTK_ALIGN_START);
    gtk_container_add(GTK_CONTAINER(box3), box3_4);
    label_3_4 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_3_4), "<b>6. Select processing mode</b>");
    gtk_box_pack_start(GTK_BOX(box3_4),label_3_4, TRUE, TRUE,10);

    buttonMode1 = gtk_button_new_with_label("Direct mapping");
    buttonMode2 = gtk_button_new_with_label("Optimisation and conservation 1");
    buttonMode3 = gtk_button_new_with_label("Optimisation and conservation 2");

    g_signal_connect(buttonMode1, "clicked", G_CALLBACK(Mode1_selected), NULL);
    g_signal_connect(buttonMode2, "clicked", G_CALLBACK(Mode2_selected), NULL);
    g_signal_connect(buttonMode3, "clicked", G_CALLBACK(Mode3_selected), NULL);

    gtk_container_add(GTK_CONTAINER(box3_4), buttonMode1);
    gtk_container_add(GTK_CONTAINER(box3_4), buttonMode2);
    gtk_container_add(GTK_CONTAINER(box3_4), buttonMode3);

    box3_5 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_widget_set_hexpand(box3_5, TRUE);
    gtk_widget_set_valign(box3_5, GTK_ALIGN_START);
    gtk_container_add(GTK_CONTAINER(box3), box3_5);
    label_3_5 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_3_5), "<b>7. Start sequences processing</b>");
    gtk_box_pack_start(GTK_BOX(box3_5),label_3_5, TRUE, TRUE,10);

    buttonRun = gtk_button_new_with_label("Run!");
    gtk_container_add(GTK_CONTAINER(box3_5), buttonRun);
    g_signal_connect(buttonRun, "clicked", G_CALLBACK(Run_process), NULL);

    box4 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(parentbox), box4);
    label_4 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_4), "<b>8. Output file containing the modified sequences</b>");
    gtk_box_pack_start(GTK_BOX(box4),label_4, TRUE, TRUE,10);

    output_1 = gtk_text_view_new();//Create the view widget (zone of display)

    gtk_widget_set_valign(output_1, GTK_ALIGN_FILL);
    gtk_box_pack_start(GTK_BOX(box4),output_1, TRUE, TRUE,10);

    box5 = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(parentbox), box5);
    label_5 = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(label_5), "<b>9. Output file containing the percentage of change of the modified sequences</b>");
    gtk_box_pack_start(GTK_BOX(box5),label_5, TRUE, TRUE,10);

    output_2 = gtk_text_view_new();//Create the view widget (zone of display)

    gtk_widget_set_valign(output_2, GTK_ALIGN_FILL);
    gtk_box_pack_start(GTK_BOX(box5),output_2, TRUE, TRUE,10);

    gtk_container_add(GTK_CONTAINER(window), parentbox);
    gtk_widget_show_all(window);//Show all widgets packed in window

    Instructions_Window();//Call the function that creates the Software Instructions window

    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);//If top right arrow of window is clicked = window "destroyed" code is quitted

    gtk_main();//Run the main loop until code is quitted

    return 0;
}




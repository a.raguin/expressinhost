#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <time.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <string.h>
#include <list>
#include <math.h>
#include <cstring>
#include <errno.h>

using   namespace  std;
using std::ifstream;
using std::ofstream;
using std::ios;

#define wobble_rate 0.35//Percent by which a rate is decreased if wobble applies
#define slow_speed_threshold 0.50//Percent of the slowest codons that are tagged as slow, calculated only on those below the average speed (averaged over the table)

int k;
int AA_num;
int cpt_codons_AA;
float top_GCN_AA;
float last_GCN_AA;
float GCN_tot;
float average_speed;
float lowest_speed;
string line1, line5;
vector<string> AA,tRNA,Codon,Corresp_codon,Symbol_Speed;
vector<float> GCN,Rank,Speed;
vector<string> AA_list;


void Process_tables()
{
    //AA_list contains all the tri-letter codes of the amino acids to be identified in the first column of the input raw tables
    AA_list.push_back("Ala");
    AA_list.push_back("Arg");
    AA_list.push_back("Asn");
    AA_list.push_back("Asp");
    AA_list.push_back("Cys");
    AA_list.push_back("Gln");
    AA_list.push_back("Glu");
    AA_list.push_back("Gly");
    AA_list.push_back("His");
    AA_list.push_back("Ile");
    AA_list.push_back("Leu");
    AA_list.push_back("Lys");
    AA_list.push_back("Met");
    AA_list.push_back("Phe");
    AA_list.push_back("Pro");
    AA_list.push_back("Ser");
    AA_list.push_back("Thr");
    AA_list.push_back("Trp");
    AA_list.push_back("Tyr");
    AA_list.push_back("Val");



    ifstream file3_1("Table_names.txt", ios::in);//Table_names.txt contains, line by line, the name of all input raw tables
    line1.clear();
    if(file3_1)
    {
        while(getline(file3_1,line1))//Read the file line by line
        {
            ifstream file3_2("Tables/"+line1, ios::in);//Open the file whose name is contained in the Table_names.txt file
            line5.clear();
            if(file3_2)
            {

                AA.clear();
                tRNA.clear();
                Codon.clear();
                Corresp_codon.clear();
                GCN.clear();
                Rank.clear();
                Speed.clear();
                Symbol_Speed.clear();

                for (k=0; k<61; k++)//All input raw tables contain 61 lines (for 61 codons)
                {
                    AA.push_back("0");//Column 1 of the input raw table
                    tRNA.push_back("0");//Column 2 of the input raw table
                    Codon.push_back("0");//Column 3 of the input raw table
                    Corresp_codon.push_back("0");//Column 5 of the input raw table
                    GCN.push_back(0.);//Column 4 of the input raw table

                    Rank.push_back(0.);//Colmun 6 to be added to the output processed table
                    Speed.push_back(0.);//Column 7 to be added to the output processed table
                    Symbol_Speed.push_back("0");//Column 8 to be added to the output processed table
                }

                k=0;
                while(getline(file3_2,line5))//Read the file line by line
                {
                    istringstream in(line5);
                    //Store the lines, column by column, in the corresponding vectors
                    in >> AA.at(k) >> tRNA.at(k) >> Codon.at(k) >> GCN.at(k) >> Corresp_codon.at(k);
                    k++;
                }

                AA_num=0;//This index allows to know with which amino acid of the list we are dealing (the first is "Ala" the second is "Arg"...)
                cpt_codons_AA=0;//For each amino acid, it counts the number of codons that have been processed
                top_GCN_AA=0.;//Highest GCN for each amino acid
                last_GCN_AA=1000000.;//Lowest GCN for each amino acid
                GCN_tot=0.;//Stores the sum of all GCN of the output processed table

                /****** We complement the input raw table for the codons with GCN = 0 and we count the total number of GCN, including the complemented ones ******/
                for(k=0; k<61; k++)//Going through the complete input raw table
                {
                    //If on the first column the amino acid is the one we are currently dealing with
                    if(AA.at(k)==AA_list.at(AA_num))
                    {

                        if(GCN.at(k)==0)//If no GCN for that codon
                        {
                            for(int t=0; t<61; t++)//In all codons of the input raw table
                            {
                                if(Codon.at(t)==Corresp_codon.at(k))//We search its corresponding codon
                                    GCN.at(k)=GCN.at(t)-wobble_rate*GCN.at(t);//And calculate its GCN using the GCN of the codon it wobbles with, and the wobbling rate
                            }
                        }

                        if(GCN.at(k)>top_GCN_AA)//If the GCN is higher than the top one
                            top_GCN_AA=GCN.at(k);
                        if(GCN.at(k)<last_GCN_AA)//If the GCN is lower than the lowest one
                            last_GCN_AA=GCN.at(k);

                        cpt_codons_AA++;//One more codon for that amino acid has been processed
                        GCN_tot = GCN_tot+GCN.at(k);
                    }

                    //If on the first column the amino acid is no longer the one we are currently dealing with
                    else if(AA.at(k)==AA_list.at(AA_num+1))
                    {

                        if(cpt_codons_AA >1)//If more than one codon code for that amino acid
                        {
                            for(int t=0; t<cpt_codons_AA; t++)//For all codons of the amino acid we are currently dealing with
                            {
                                if(top_GCN_AA != last_GCN_AA)
                                    Rank.at(k-cpt_codons_AA+t)=(GCN.at(k-cpt_codons_AA+t)-last_GCN_AA)/(top_GCN_AA-last_GCN_AA);//The "RANK" of the codon is calculated using its GCN, the highest and the lowest GCN of that amino acid
                                else
                                    Rank.at(k-cpt_codons_AA+t)=1.0;//If the top and lowest codons have the same GCN, the "RANK" of the codon is 1
                            }
                        }

                        if(cpt_codons_AA==1)//If only one codon codes for that amino acid
                            Rank.at(k-cpt_codons_AA)=1;


                        AA_num++;//Switch to the next amino acid of the amino acid list
                        cpt_codons_AA=0;//Reset the counter of codons for each amino acids
                        top_GCN_AA=0.;//Reset the value for highest GCN
                        last_GCN_AA=1000000.;//Reset value for lowest GCN
                        k--;//Step one line back such that this line itself will be processed by entering the first "If condition" of the "For loop"
                    }

                    //If we reach the last line of the input raw table
                    if(k==60)
                    {
                        if(cpt_codons_AA >1)//If more than one codon code for that amino acid
                        {

                            for(int t=0; t<cpt_codons_AA; t++)//For all codons of the amino acid we are currently dealing with
                            {
                                if(top_GCN_AA != last_GCN_AA)
                                    Rank.at(k+1-cpt_codons_AA+t)=(GCN.at(k+1-cpt_codons_AA+t)-last_GCN_AA)/(top_GCN_AA-last_GCN_AA);//The "RANK" of the codon is calculated using its GCN, the highest and the lowest GCN of that amino acid
                                else
                                    Rank.at(k+1-cpt_codons_AA+t)=1.0;//If the top and lowest codons have the same GCN, the "RANK" of the codon is 1
                            }

                        }

                        if(cpt_codons_AA==1)//If only one codon codes for that amino acid
                            Rank.at(k+1-cpt_codons_AA)=1;
                    }

                }

                /****** Calculate the "SPEED" and the average "SPEED" over the entire table ******/
                average_speed=0.;
                for(k=0; k<61; k++)
                {
                    Speed.at(k)=GCN.at(k)/GCN_tot;
                    average_speed = average_speed+Speed.at(k);
                }
                average_speed=average_speed/61.0;

                /****** Search the lowest "SPEED" over the entire table ******/
                lowest_speed=10000000.;
                for(k=0; k<61; k++)
                {
                    if(Speed.at(k)<lowest_speed)
                        lowest_speed=Speed.at(k);
                }

                /****** Tag the codons of low "SPEED" over the entire table ******/
                for(k=0; k<61; k++)
                {
                    //If codon's SPEED is below the threshold set by the slow_speed_threshold.
                    //That threshold is a limit SPEEd value, independent from the number of codons that can fall in that category.
                    if(Speed.at(k)<lowest_speed+(slow_speed_threshold*(average_speed-lowest_speed)))
                        Symbol_Speed.at(k)="S";
                }

                ofstream file3_3("Tables/Processed_"+line1, ios::out);//Output processed table with name derived from input raw table
                ofstream file3_4("Tables/Processed_"+line1, ios::app);
                for(k=0; k<61; k++)
                    file3_4 << AA.at(k)<<'\t' << tRNA.at(k)<<'\t' << Codon.at(k)<<'\t' << GCN.at(k)<<'\t' << Corresp_codon.at(k)<<'\t' <<Rank.at(k)<<'\t' <<Speed.at(k)<<'\t' <<Symbol_Speed.at(k)<<endl;

            }
        }
    }

}
